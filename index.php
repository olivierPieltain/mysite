<html lang="en"><head>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="content-script-type" content="text/javascript">
        <meta http-equiv="content-style-type" content="text/css">
        <meta http-equiv="content-language" content="en">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="author" content="Olivier Pieltain">	
        <meta name="description" content="I'm Olivier Pieltain, a webdeveloper / creative programmer with MCSD Web Applications certification.">
        <meta name="keywords" content="Olivier Pieltain, Interactive Resume, C# programmer, Web developer, Startup, Interactive CV, Resume, CV, HRMatches, Algorithms, PHP, MySQL, OOP">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="14 days">

        <title>Olivier Pieltain - Web Developer - Interactive Resume</title>

        <!-- Bootstrap core CSS -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

    </head>
    <body data-spy="scroll" data-target="#navbar-example" class="fixed">	 



        <div id="top" class="jumbotron" data-src="css/images/olivier.jpg" data-position="center right" style="height: 619px;">
            <div class="container">
                <h1>Olivier Pieltain</h1>
                <p class="lead">Interactive resume</p>
            </div>

            <div class="overlay"></div>

            <a href="#profile" class="scroll-down">	
                <span class="glyphicon glyphicon-chevron-down"></span>
            </a>
        </div>

        <nav class="navbar navbar-default" id="navbar-example" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#profile">Profile</a></li>
                    <li class=""><a href="#experiences">Experiences</a></li>
                    <li class=""><a href="#abilities">Abilities</a></li>
                    <li class=""><a href="#contact">Contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>

        <div class="background-white">
            <div id="profile" class="container">
                <h2>Profile</h2>
                <p class="lead">I'm a Webdeveloper</p>

                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <h3>About me</h3>
                        <p>
                            I am a web developer. Junior programmer with good knowledge of web development. I love scalability and good design and I also stand for quality. I like working in a team, even if you have to learn a lot by yourself, you shouldn't refuse help from more experimented than you. 	</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="css/images/olivier.jpg" alt="It should be my picture, normally..." width="246" height="246">
                    </div>
                    <div class="col-md-4">
                        <h3>Details</h3>
                        <p>
                            <strong>Name:</strong><br>
                            Olivier Pieltain<br>
                            <strong>Age:</strong><br>
                            28 years<br>
                            <strong>Location:</strong><br>
                            Brussels, Belgium, Earth		</p>

                    </div>
                </div>			</div>	
        </div>	

        <div id="experiences" class="container">
            <h2>Experiences</h2>
            <p class="lead">
                “What I've done && What I've learned.”
            </p>

            <hr>



            <h3>Educations</h3>
            <div class="experience row">
                <div class="col-md-4">
                    <h4>EPFC</h4>
                    <p class="experience-period">
                        Sep 2014				- 
                        Jun 2017			</p>
                </div>
                <div class="col-md-8">
                    <p>
                        <strong>Bachelor - IT Management </strong>
                        <span class="hidden-phone">
                            The education was mainly Java-based programming, but I also learned about SQL, PHP, C++, C#, MVC, UML and more. Here I learned the importance of OOP and MVC.				</span>
                        <span class="experience-details">
                            <span class="location">
                                <span class="glyphicon glyphicon-map-marker"></span>
                                Woluwe-st-Pierre					</span>
                        </span>
                    </p>
                </div>
            </div>

            <div class="experiences">
                <div class="experience row">
                    <div class="col-md-4">
                        <h4>Business Training</h4>
                        <p class="experience-period">
                            Dec 2015				- 
                            Jun 2016			</p>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <strong>Microsoft Certified Solutions Developer - Web Applications</strong>
                            <span class="hidden-phone">
                                This education was providing me a deep level of knowledge about the .Net framework, starting with C# following with MVC.net and finishing with web-services and deployement. For each module (3 in total), I earned the certification.    				</span>
                            <span class="experience-details">
                                <span class="location">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                    Brussels					</span>
                            </span>
                        </p>
                    </div>
                </div>



                <div class="experience row">
                    <div class="col-md-4">
                        <h4>Febelfin</h4>
                        <p class="experience-period">
                            Mar 2009				- 
                            Apr 2009			</p>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <strong>Finance - Willems Law</strong>
                            <span class="hidden-phone">
                                Three weeks intensive education about ethical practice of banking activities, finishing by a certificate who allows me to work in any belgian bank.				</span>
                            <span class="experience-details">
                                <span class="location">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                    Brussels					</span>
                            </span>
                        </p>
                    </div>
                </div>

                <div class="experience row">
                    <div class="col-md-4">
                        <h4>ITM</h4>
                        <p class="experience-period">
                            Sep 2002				- 
                            Jun 2007			</p>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <strong>Highschool - Accountancy</strong>
                            <span class="hidden-phone">
                                I graduated highschool, where I enjoyed learning accountancy, finance and law.				</span>
                            <span class="experience-details">
                                <span class="location">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                    Morlanwelz					</span>
                            </span>
                        </p>
                    </div>
                </div>


            </div>



            <hr>

            <h3>Careers</h3>


            <div class="experiences">


                <div class="experience row">
                    <div class="col-md-4">
                        <h4>Bpost Bank</h4>
                        <p class="experience-period">
                            Jan 2012				- 
                            Mar 2015			</p>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <strong>Financial advisor</strong>
                            <span class="hidden-phone">
                                As financial advisor, I handled specific complaints in both B2B & B2C context. I was also the first point of contact of my front office colleagues.				</span>
                            <span class="experience-details">
                                <span class="location">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                    Brussels					</span>


                            </span>
                        </p>
                    </div>
                </div>


                <div class="experience row">
                    <div class="col-md-4">
                        <h4>Bpost Bank</h4>
                        <p class="experience-period">
                            Jan 2008				- 
                            Jan 2012			</p>
                    </div>
                    <div class="col-md-8">
                        <p>
                            <strong>Customer advisor</strong>
                            <span class="hidden-phone">
                                First person of contact for the customers, I enjoyed a lot to interact with customers and finding the financial products who fit the best with their investor profile.				</span>
                            <span class="experience-details">
                                <span class="location">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                    Brussels					</span>


                            </span>
                        </p>
                    </div>
                </div>


            </div>

        </div>

        <div class="background-white">
            <div id="abilities" class="container">
                <h2>Abilities</h2>
                <p class="lead">
                    "What I know && What I master"
                </p>

                <hr>

                <h3>Skills</h3>

                <div class="row">


                    <div class="col-md-6">
                        <ul class="no-bullets">


                            <li>
                                <span class="ability-title">CSS(3)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>

                                </span>
                            </li>



                            <li>
                                <span class="ability-title">HTML(5)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>



                                </span>
                            </li>



                            <li>
                                <span class="ability-title">JSON</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>



                                </span>
                            </li>



                            <li>
                                <span class="ability-title">MySQL</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>

                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>




                                </span>
                            </li>



                            <li>
                                <span class="ability-title">PHP</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">XML</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>
         


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Bootstrap Framework</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                  






                            <li>
                                <span class="ability-title">JQuery</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Java</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


        
                                    <span class="glyphicon glyphicon-star "></span>
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Javascript</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                   
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">C#</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="no-bullets">



            



                            <li>
                                <span class="ability-title">MVC Pattern</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                         



                            <li>
                                <span class="ability-title">Object Orientated Programming</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                  



                       



                  



                            <li>
                                <span class="ability-title">Winforms</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                   
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>
                             <li>
                                <span class="ability-title">Asp.net MVC</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                   
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>
                             <li>
                                <span class="ability-title">Entity Framework</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                   
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>
                             <li>
                                <span class="ability-title">UML</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                   
                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>


                        </ul>
                    </div>


                </div>

           

                <hr>

                <h3>Languages</h3>

                <div class="row">


                    <div class="col-md-6">
                        <ul class="no-bullets">


                            <li>
                                <span class="ability-title">French (Langue maternelle)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">English (Daily use with marvellous french accent)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="no-bullets">


                            <li>
                                <span class="ability-title">Dutch (Juist een beetje)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                        </ul>
                    </div>


                </div>

                <hr>

                <h3>Tools</h3>

                <div class="row">

                    <div class="col-md-6">
                        <ul class="no-bullets">


                            <li>
                                <span class="ability-title">FileZilla (3 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                  <span class="glyphicon glyphicon-star "></span>
                                  
                                  <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Google Chrome (5 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                           <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">MS Office (5 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                          <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Mac (5 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                       <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Netbeans (3 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                          <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Webkit browsers (6 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                  


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Windows (10+ years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Visual Studio (3 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                  


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="no-bullets">



                            <li>
                                <span class="ability-title">Firefox (3 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                  


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Git(4 year)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Internet Explore 6+ (8 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">WAMP (3 year)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">EasyPhp (1 year)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                           <span class="glyphicon glyphicon-star "></span>
                                  


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>



                            <li>
                                <span class="ability-title">Codeblocks (2 years)</span>
                                <span class="ability-score">


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                    <span class="glyphicon glyphicon-star filled"></span>


                                            <span class="glyphicon glyphicon-star "></span>
                                  


                                    <span class="glyphicon glyphicon-star "></span>


                                </span>
                            </li>






                        </ul>
                    </div>

                </div>			</div>
        </div>




        <div class="background-gray">
            <div id="contact" class="container">
                <h2>Contact</h2>
                <p class="lead">
                    "How to reach me"
                </p>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <ul class="no-bullets">
                            <li>
                                
                                <a  href="https://be.linkedin.com/in/olivier-pieltain-67426111a" style="margin-right: 20px" target="_blank" >
                                  <span style="margin: 20px" class="fa fa-linkedin" aria-hidden="true"></span>
                                  LinkedIn 
                                    				</a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="no-bullets">

                            <li>
                                
                                <a style="unicode-bidi:bidi-override; direction: rtl;" href="mailto:moc.liamg@niatleipreivilo">
                                    
                                    <span style="unicode-bidi:bidi-override; direction: rtl;">
                                        moc.liamg@niatleipreivilo	<span style="margin: 20px" class="fa fa-envelope"></span>				</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <hr>

       			</div>
        </div>

	
        
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="https://use.fontawesome.com/09e12b0444.js"></script>



        <iframe id="rufous-sandbox" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" style="position: absolute; visibility: hidden; display: none; width: 0px; height: 0px; padding: 0px; border: none;"></iframe>
    </body>
</html>